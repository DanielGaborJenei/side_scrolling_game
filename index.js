'use strict';

const http = require('http');
const ecstatic = require('ecstatic')({
  root: '.',
});

const DEFAULT_PORT = 3000;

const PORT = process.env.PORT || DEFAULT_PORT;
http.createServer(ecstatic).listen(PORT);

console.log(`Listening on :${DEFAULT_PORT}`);
