import * as Utilities from './utilities';
import { CANVAS as originalCANVAS, RESOURCES_PATH } from './configuration';

let CANVAS = originalCANVAS;

jest.mock('./configuration.ts', () => ({
  CANVAS: {},
  RESOURCES: { test: 'test.png' },
  RESOURCES_PATH: 'resources',
}));

CANVAS.height = 200;
CANVAS.width = 200;

const mockMath = Object.create(global.Math);
mockMath.random = () => 0;
global.Math = mockMath;

jest.useFakeTimers();

describe('getResource from', () => {
  it('valid test path', () => {
    expect(Utilities.getResource('test')).toBe(
      RESOURCES_PATH + '/' + 'test.png'
    );
  });
  it('invalid test path', () => {
    expect(Utilities.getResource('nope')).toBe(
      RESOURCES_PATH + '/' + undefined
    );
  });
});

describe('getRandomRange', () => {
  it('should return value in range', () => {
    mockMath.random = () => 0;
    expect(Utilities.getRandomRange()).toBeGreaterThanOrEqual(-1);
    mockMath.random = () => 0.99999;
    expect(Utilities.getRandomRange()).toBeLessThan(1);
  });
});

describe('getCorrectedSpeed', () => {
  describe('should not change speed', () => {
    it('within the specified range at ', () => {
      expect(Utilities.getCorrectedSpeed(5, 7, 13, 10, 0)).toBe(7);
    });
    it('highLimit position', () => {
      expect(Utilities.getCorrectedSpeed(10, 7, 13, 10, 0)).toBe(7);
    });
    it('lowLimit position', () => {
      expect(Utilities.getCorrectedSpeed(0, 7, 13, 10, 0)).toBe(7);
    });
  });
  describe('should change speed', () => {
    it('to negative of newSpeed above highLimit', () => {
      expect(Utilities.getCorrectedSpeed(12, 7, 13, 10, 0)).toBe(-13);
    });
    it('below lowLimit to newSpeed', () => {
      expect(Utilities.getCorrectedSpeed(-3, 7, 13, 10, 0)).toBe(13);
    });
  });
});

describe('limiter(value, minLimit, maxLimit)', () => {
  describe('should not change value', () => {
    it('within the specified range', () => {
      expect(Utilities.limiter(7, 10, 1)).toBe(7);
    });
    it('at maxLimit', () => {
      expect(Utilities.limiter(10, 10, 1)).toBe(10);
    });
    it('at minLimit', () => {
      expect(Utilities.limiter(0, 10, 1)).toBe(1);
    });
  });
  describe('should return', () => {
    it('maxLimit if value is above range', () => {
      expect(Utilities.limiter(23, 10, 1)).toBe(10);
    });
    it('minLimit if value is below range', () => {
      expect(Utilities.limiter(-35, 10, 1)).toBe(1);
    });
  });
});

describe('calculateLimitedUnitPosition', () => {
  it('calls limiter', () => {
    let mockUnit = {
      sprite: { x: 300, y: 300, position: { x: 300, y: 300 } } as PIXI.Sprite,
      velocity: { x: 10, y: 10 },
    } as Unit;
    let mockLimit: PositionLimit = { up: 60, down: 60, left: 60, right: 60 };
    const limiter = jest.spyOn(Utilities, 'limiter');

    Utilities.calculateLimitedUnitPosition(mockUnit as Unit, mockLimit);

    expect(limiter).toHaveBeenCalledTimes(2);
    expect(limiter).toHaveBeenCalledWith(
      mockUnit.sprite.position.x + mockUnit.velocity.x,
      CANVAS.width - mockLimit.right,
      mockLimit.left
    );
    expect(limiter).toHaveBeenCalledWith(
      mockUnit.sprite.y + mockUnit.velocity.y,
      CANVAS.height - mockLimit.down,
      mockLimit.up
    );
  });
});

describe('setLimitedUnitPosition', () => {
  it('calls calculateLimitedUnitPosition() and position.set()', () => {
    let mockUnit: Unit = {
      sprite: { x: 300, y: 300, position: { x: 300, y: 300 } } as PIXI.Sprite,
      velocity: { x: 10, y: 10 },
    };
    let mockLimit: PositionLimit = { up: 60, down: 60, left: 60, right: 60 };
    const calculateLimitedUnitPosition = jest.spyOn(
      Utilities,
      'calculateLimitedUnitPosition'
    );
    mockUnit.sprite.position.set = jest.fn();

    Utilities.setLimitedUnitPosition(mockUnit, mockLimit);

    expect(calculateLimitedUnitPosition).toHaveBeenCalledWith(
      mockUnit,
      mockLimit
    );

    expect(mockUnit.sprite.position.set).toHaveBeenCalled();
  });
});

describe('newPixiSprite', () => {
  it('calls fromImage', () => {
    PIXI.Sprite = ({} as unknown) as typeof PIXI.Sprite;
    PIXI.Sprite.fromImage = jest.fn();

    const fromImage = jest.spyOn(PIXI.Sprite, 'fromImage');

    Utilities.newPixiSprite('test');

    expect(fromImage).toHaveBeenCalled();
  });
});

describe('newPixiTexture', () => {
  it('calls fromImage', () => {
    PIXI.Texture = ({} as unknown) as typeof PIXI.Texture;
    PIXI.Texture.fromImage = jest.fn();

    const fromImage = jest.spyOn(PIXI.Texture, 'fromImage');

    Utilities.newPixiTexture('test');

    expect(fromImage).toHaveBeenCalled();
  });
});

describe('createText', () => {
  it('calls PIXI.Text', () => {
    PIXI.Text = (jest.fn(() => ({
      anchor: {
        set: () => {},
      },
      position: { set: () => {}, x: 0, y: 0 },
    })) as unknown) as typeof PIXI.Text;

    PIXI.Sprite = ({
      position: {
        set: () => {},
        x: 0,
        y: 0,
      },
    } as unknown) as typeof PIXI.Sprite;

    Utilities.createText('text', {
      size: null,
      fill: null,
      align: null,
      anchor: { x: 0, y: 0 },
      position: { x: 1, y: 1 },
    });

    expect(PIXI.Text).toHaveBeenCalled();
  });
});

describe('fadeStep', () => {
  it('calls setTimeout and removeChild', () => {
    let mockContainer = {} as PIXI.Container;
    mockContainer.removeChild = (jest.fn(
      () => ({})
    ) as unknown) as typeof PIXI.Container.prototype.removeChild;

    const sprite = { alpha: 1, scale: { set: () => {} } } as PIXI.Sprite;

    const step: Step = {
      sprite,
      container: mockContainer,
      duration: 2000,
      delay: 1000 / 30,
      fadeAmount: 30 / 2000,
      doesScaleOut: true,
      finalStep: () => {},
    };

    Utilities.fadeStep(step);
    expect(setTimeout).toHaveBeenCalled();

    step.fadeAge = step.duration;
    Utilities.fadeStep(step);

    expect(mockContainer.removeChild).toHaveBeenCalled();
  });
});

describe('removeChildWithFadeOut', () => {
  let mockSprite: PIXI.Sprite;
  let mockContainer: PIXI.Container;
  const duration = 2000;
  let doesScaleOut;
  const finalStep = (): void => {};

  it('calls setTimeout in case doesScaleOut is defined', () => {
    mockSprite = {} as PIXI.Sprite;
    mockContainer = {} as PIXI.Container;
    doesScaleOut = true;

    Utilities.removeChildWithFadeOut(
      mockSprite,
      mockContainer,
      duration,
      doesScaleOut,
      finalStep
    );

    expect(setTimeout).toHaveBeenCalled();
  });

  it('calls setTimeout in case doesScaleOut is NOT defined', () => {
    let mockSprite = {} as PIXI.Sprite;
    let mockContainer = {} as PIXI.Container;
    let doesScaleOut;

    Utilities.removeChildWithFadeOut(
      mockSprite,
      mockContainer,
      duration,
      doesScaleOut,
      finalStep
    );

    expect(setTimeout).toHaveBeenCalled();
  });
});

describe('shiftPositionBy', () => {
  it('calls PIXI.position.set', () => {
    let mockSprite = { position: {}, x: 0, y: 0 } as PIXI.Sprite;
    mockSprite.position.set = jest.fn();

    let mockVelocity = { x: 10, y: 10 };

    Utilities.shiftPositionBy(mockSprite, mockVelocity)();

    expect(mockSprite.position.set).toHaveBeenCalled();
  });
});

describe('explosion', () => {
  it('calls setTimeout', () => {
    let app = ({ ticker: { add: () => {} } } as unknown) as PIXI.Application;
    let mockContainer = {} as PIXI.Container;
    mockContainer.addChild = (jest.fn(
      () => ({})
    ) as unknown) as typeof PIXI.Container.prototype.addChild;
    let mockPosition = {
      set: () => {},
      x: 0,
      y: 0,
    };
    let maxParticles = 100;
    let speed = 5;
    let duration = 2000;

    const newPixiSprite = jest
      .spyOn(Utilities, 'newPixiSprite')
      .mockImplementation(
        (): PIXI.Sprite => {
          return { position: { set: () => {}, x: 0, y: 0 } } as PIXI.Sprite;
        }
      );
    const removeChildWithFadeOut = jest
      .spyOn(Utilities, 'removeChildWithFadeOut')
      .mockImplementation(() => {});

    Utilities.explosion(
      app,
      mockContainer,
      mockPosition,
      maxParticles,
      speed,
      duration
    );

    expect(newPixiSprite).toHaveBeenCalledTimes(maxParticles);
    expect(removeChildWithFadeOut).toHaveBeenCalledTimes(maxParticles);
  });
});

describe('addUnit', () => {
  it('calls addChild', () => {
    let mockContainer = {} as PIXI.Container;
    mockContainer.addChild = (jest.fn(
      () => ({})
    ) as unknown) as typeof PIXI.Container.prototype.addChild;

    let mockPosition = {
      set: () => {},
      x: 0,
      y: 0,
    };
    let mockVelocity = { x: 0, y: 0 };

    jest.spyOn(Utilities, 'newPixiSprite').mockImplementation(
      (): PIXI.Sprite => {
        return {
          anchor: { set: () => {} },
          x: 0,
          y: 0,
          position: { set: () => {}, x: 0, y: 0 },
        } as PIXI.Sprite;
      }
    );

    Utilities.addUnit(mockContainer, 'test', [], mockPosition, mockVelocity);

    expect(mockContainer.addChild).toHaveBeenCalled();
  });
});

describe('addBackgroundChild', () => {
  it('calls addChild and PIXI.extras.TilingSprite', () => {
    PIXI.extras = ({} as unknown) as typeof PIXI.extras;
    PIXI.extras.TilingSprite = (jest.fn(
      () => ({})
    ) as unknown) as typeof PIXI.extras.TilingSprite;

    let mockContainer = {} as PIXI.Container;

    mockContainer.addChild = (jest.fn(
      () => ({})
    ) as unknown) as typeof PIXI.Container.prototype.addChild;

    Utilities.addBackgroundChild({} as PIXI.Texture, mockContainer);

    expect(PIXI.extras.TilingSprite).toHaveBeenCalled();
    expect(mockContainer.addChild).toHaveBeenCalled();
  });
});

describe('collisionDetection', () => {
  let sprite1: PIXI.Sprite;
  let sprite2: PIXI.Sprite;
  const SIZE1 = 13;
  const SIZE2 = 9;

  describe('should return true if the tested objects collide', () => {
    beforeEach(() => {
      sprite1 = {
        position: { x: 0, y: 0 },
        width: SIZE1,
        height: SIZE1,
      } as PIXI.Sprite;
      sprite2 = {
        position: { x: SIZE1, y: SIZE1 },
        width: SIZE2,
        height: SIZE2,
      } as PIXI.Sprite;
    });

    describe('vertically', () => {
      it('from top', () => {
        sprite1.position.y = 1;
        sprite1.position.x = SIZE1;

        expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(true);
      });
      it('from bottom', () => {
        sprite1.position.y = SIZE1 + SIZE2 - 1;
        sprite1.position.x = SIZE1;

        expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(true);
      });
    });

    describe('horizontally', () => {
      it('from left', () => {
        sprite1.position.x = 1;
        sprite1.position.y = SIZE1;

        expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(true);
      });
      it('from right', () => {
        sprite1.position.x = SIZE1 + SIZE2 - 1;
        sprite1.position.y = SIZE1;

        expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(true);
      });
    });
  });

  describe('should return false if the tested objects', () => {
    beforeEach(() => {
      sprite1 = {
        position: { x: 0, y: 0 },
        width: SIZE1,
        height: SIZE1,
      } as PIXI.Sprite;
      sprite2 = {
        position: { x: SIZE1, y: SIZE1 },
        width: SIZE2,
        height: SIZE2,
      } as PIXI.Sprite;
    });

    it('are snuggled vertically', () => {
      sprite1.position.x = SIZE1;

      expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(false);
    });
    it('are snuggled horizontally', () => {
      sprite1.position.y = SIZE1;

      expect(Utilities.collisionDetection(sprite1, sprite2)).toBe(false);
    });
  });
});
