import { getCorrectedSpeed, getRandomRange, newPixiSprite } from '../utilities';
import { CANVAS, TIMEOUTS } from '../configuration';

let menuShip: Unit;

export const addMenuShipChild = (container: PIXI.Container): void => {
  menuShip = {
    sprite: newPixiSprite('ship'),
    velocity: { x: 0, y: 0 },
    alive: true,
  };

  menuShip.sprite.position.set(CANVAS.width * 0.625, CANVAS.height * 0.6);
  container.addChild(menuShip.sprite);
};

export const menuShipAnimationInit = (container: PIXI.Container): void => {
  const menuShipAnimation = (): void => {
    menuShip.velocity.x = getRandomRange() * 2;
    menuShip.velocity.y = getRandomRange() * 2;

    if (container.visible) {
      setTimeout(menuShipAnimation, TIMEOUTS.menuShipDirectionChange);
    }
  };

  menuShip.sprite.position.set(CANVAS.width * 0.625, CANVAS.height * 0.6);
  menuShip.velocity.x = 2;
  menuShip.velocity.y = 0;

  setTimeout(menuShipAnimation, TIMEOUTS.menuShipDirectionChange);
};

export const menuShipBoundaryCheck = (): void => {
  menuShip.velocity.x = getCorrectedSpeed(
    menuShip.sprite.position.x,
    menuShip.velocity.x,
    1,
    CANVAS.width * 0.8,
    CANVAS.width * 0.3
  );
  menuShip.velocity.y = getCorrectedSpeed(
    menuShip.sprite.position.y,
    menuShip.velocity.y,
    1,
    CANVAS.height * 0.6,
    CANVAS.height * 0.3
  );

  menuShip.sprite.position.x += menuShip.velocity.x;
  menuShip.sprite.position.y += menuShip.velocity.y;
};
