import {
  addUnit,
  collisionDetection,
  createText,
  newPixiSprite,
} from '../utilities';
import { INFO_TEXT_STYLE, CANVAS, MESSAGES } from '../configuration';

export class CollisionTestScene {
  state: State;

  container: PIXI.Container = new PIXI.Container();
  background: PIXI.Sprite = newPixiSprite('openingBackground');

  enemies: Unit[] = [];
  ships: Unit[] = [];

  constructor(state: State) {
    this.state = state;
    this.background.anchor.set(0.5, 0.5);
    this.background.position.set(CANVAS.width * 0.5, CANVAS.height * 0.5);
    this.container.addChild(this.background);

    this.container.addChild(createText(MESSAGES['PRESS_ESC'], INFO_TEXT_STYLE));

    const GRID = {
      x: [100, 300, 500, 700],
      y: [175, 375],
    };
    const SHIP_POS: { x: number; y: number }[] = [
      { x: GRID.x[0], y: GRID.y[0] },
      { x: GRID.x[1], y: GRID.y[0] },
      { x: GRID.x[2], y: GRID.y[0] },
      { x: GRID.x[3], y: GRID.y[0] },
      { x: GRID.x[0], y: GRID.y[1] },
      { x: GRID.x[1], y: GRID.y[1] },
      { x: GRID.x[2], y: GRID.y[1] },
      { x: GRID.x[3], y: GRID.y[1] },
    ];

    const ENEMY_POS: { x: number; y: number }[] = [
      { x: SHIP_POS[0].x - 60, y: SHIP_POS[0].y },
      { x: SHIP_POS[1].x + 80, y: SHIP_POS[1].y },
      { x: SHIP_POS[2].x, y: SHIP_POS[2].y - 40 },
      { x: SHIP_POS[3].x, y: SHIP_POS[3].y + 52 },
      { x: SHIP_POS[4].x - 60, y: SHIP_POS[4].y - 40 },
      { x: SHIP_POS[5].x + 80, y: SHIP_POS[5].y + 52 },
      { x: SHIP_POS[6].x + 80, y: SHIP_POS[6].y - 40 },
      { x: SHIP_POS[7].x - 60, y: SHIP_POS[7].y + 52 },
    ];

    SHIP_POS.forEach(e => this.addShip(e.x, e.y));
    ENEMY_POS.forEach(e => this.addEnemy(e.x, e.y));

    this.container.visible = false;
    state.app.stage.addChild(this.container);
  }

  addShip = (posX: number, posY: number) => {
    addUnit(
      this.container,
      'ship',
      this.ships,
      { x: posX, y: posY },
      { x: 0, y: 0 }
    );
  };
  addEnemy = (posX: number, posY: number) => {
    addUnit(
      this.container,
      'enemy',
      this.enemies,
      { x: posX, y: posY },
      { x: 0, y: 0 }
    );
  };

  checkCollisions = () => {
    this.ships.forEach((ship: Unit) => {
      ship.sprite.alpha = 1;
    });
    this.enemies.forEach((enemy: Unit) => {
      enemy.sprite.alpha = 1;
    });

    this.ships.forEach((ship: Unit) => {
      this.enemies.forEach((enemy: Unit) => {
        if (collisionDetection(ship.sprite, enemy.sprite)) {
          ship.sprite.alpha = 0.3;
          enemy.sprite.alpha = 0.3;
        }
      });
    });
  };

  repositionShips = (): void => {
    this.ships.forEach(ship => {
      ship.sprite.position.set(
        ship.sprite.position.x + this.state.control.direction.x,
        ship.sprite.position.y + this.state.control.direction.y
      );
    });
  };

  checkInputEvents = (nextScene: Scene) => {
    if (this.state.control.activated.quit) {
      this.state.control.activated.quit = false;
      this.container.visible = false;
      this.state.loop = nextScene.getLoop(nextScene);
    }
  };

  getLoop(nextScene: Scene): Function {
    this.container.visible = true;

    return () => {
      this.repositionShips();
      this.checkCollisions();
      this.checkInputEvents(nextScene);
    };
  }
}
