import {
  CANVAS,
  EXPLOSION,
  MISSION_MAIN_TEXT_STYLE,
  LEVEL_TEXTS,
  MESSAGES,
  PARALLAX_BACKGROUND_SPEED,
  PARALLAX_FOREGROUND_SPEED,
  TIMEOUTS,
} from '../configuration';

import {
  addBackgroundChild,
  addUnit,
  collisionDetection,
  createText,
  explosion,
  getCorrectedSpeed,
  getRandomRange,
  newPixiTexture,
  setLimitedUnitPosition,
} from '../utilities';

export class MissionScene {
  state: State;

  container: PIXI.Container = new PIXI.Container();
  parallaxBackground = addBackgroundChild(
    newPixiTexture('cityBackground'),
    this.container
  );
  parallaxForeground = addBackgroundChild(
    newPixiTexture('cityForeground'),
    this.container
  );

  enemies: Unit[] = [];
  rockets: Unit[] = [];

  //TODO: reserved for future game functionalities (e.g. allied drones)
  ships: Unit[] = [];
  ship: Unit;

  constructor(state: State) {
    this.state = state;
    this.container.visible = false;
    state.app.stage.addChild(this.container);

    addUnit(this.container, 'ship', this.ships, { x: 0, y: 0 }, { x: 0, y: 0 });
    this.ship = this.ships[0];
  }

  getGameText = (message: string): PIXI.Text =>
    createText(message, MISSION_MAIN_TEXT_STYLE);

  removeText = (textToRemove: PIXI.Text) => {
    this.container.removeChild(textToRemove);
  };

  spawnEnemy = () => {
    addUnit(
      this.container,
      'enemy',
      this.enemies,
      {
        x: CANVAS.width,
        y: CANVAS.height * Math.random(),
      },
      {
        x: this.state.level.enemy.initialSpeed,
        y: 0,
      }
    );
    if (this.ship.alive) {
      setTimeout(this.spawnEnemy, this.state.level.enemy.spawnDelay);
    }
  };

  turretPosition = () => ({
    x: this.ship.sprite.position.x + this.ship.sprite.width / 2 + 5,
    y: this.ship.sprite.position.y + this.ship.sprite.height / 2 + 5,
  });

  launchPlayerProjectile = () => {
    if (this.ship.alive) {
      addUnit(this.container, 'rocket', this.rockets, this.turretPosition(), {
        x: 15,
        y: 0,
      });
      if (this.ship.alive) {
        setTimeout(this.launchPlayerProjectile, this.state.level.fireDelay);
      }
    }
  };

  randomizeEnemyMovement = () => {
    this.enemies.forEach((enemy: Unit) => {
      enemy.velocity.x =
        this.state.level.enemy.speed * (getRandomRange() - 1) + 2;
      enemy.velocity.y = getRandomRange() * this.state.level.enemy.speed;
    });
    if (this.ship.alive) {
      setTimeout(
        this.randomizeEnemyMovement,
        this.state.level.enemy.dirChangeDelay
      );
    }
  };

  checkRocketBoundary = (): void => {
    this.rockets.forEach((rocket: Unit, i) => {
      rocket.sprite.position.set(
        rocket.sprite.position.x + rocket.velocity.x,
        rocket.sprite.position.y + rocket.velocity.y
      );

      if (rocket.sprite.x > CANVAS.width) {
        this.container.removeChild(rocket.sprite);
        delete this.rockets[i];
      }
    });

    this.rockets = this.rockets.filter(e => e);
  };

  checkEnemyBoundary = (): void => {
    this.enemies.forEach((enemy: Unit, i) => {
      enemy.velocity.y = getCorrectedSpeed(
        enemy.sprite.position.y,
        enemy.velocity.y,
        4,
        CANVAS.height * 0.9,
        CANVAS.height * 0.1
      );

      enemy.sprite.position.y += enemy.velocity.y;
      enemy.sprite.position.x += enemy.velocity.x;

      if (enemy.sprite.position.x < 0) {
        this.container.removeChild(enemy.sprite);
        delete this.enemies[i];
      }
    });

    this.enemies = this.enemies.filter(e => e);
  };

  checkRocketCollision = (): void => {
    this.enemies.forEach((enemy: Unit, i) => {
      this.rockets.forEach((rocket: Unit, j) => {
        if (collisionDetection(rocket.sprite, enemy.sprite)) {
          explosion(
            this.state.app,
            this.container,
            {
              x: enemy.sprite.position.x,
              y: enemy.sprite.position.y,
            },
            EXPLOSION.particles.enemy,
            2,
            EXPLOSION.duration.enemy
          );

          this.container.removeChild(enemy.sprite);
          delete this.enemies[i];
          this.container.removeChild(rocket.sprite);
          delete this.rockets[j];
        }
      });
    });

    this.enemies = this.enemies.filter(e => e);
    this.rockets = this.rockets.filter(e => e);
  };

  checkShipCollision = (nextScene: Scene): void => {
    this.enemies.forEach((enemy: Unit) => {
      if (
        collisionDetection(this.ship.sprite, enemy.sprite) &&
        this.ship.alive
      ) {
        let endText = this.getGameText('GAME OVER');

        this.ship.alive = false;
        this.container.removeChild(this.ship.sprite);

        explosion(
          this.state.app,
          this.container,
          {
            x: this.ship.sprite.position.x,
            y: this.ship.sprite.position.y,
          },
          EXPLOSION.particles.ship,
          1,
          EXPLOSION.duration.ship
        );
        this.container.addChild(endText);

        const gameOver = (): void => {
          this.enemies.forEach((enemy: Unit) =>
            this.container.removeChild(enemy.sprite)
          );
          this.enemies = [];
          this.rockets.forEach((rocket: Unit) =>
            this.container.removeChild(rocket.sprite)
          );
          this.rockets = [];
          this.container.removeChild(endText);

          this.container.visible = false;
          this.state.loop = nextScene.getLoop();
        };

        setTimeout(gameOver, TIMEOUTS.endScene);
      }
    });
  };

  repositionShip = (): void => {
    this.ship.velocity = {
      x: this.state.control.direction.x * this.state.level.shipSpeed,
      y: this.state.control.direction.y * this.state.level.shipSpeed,
    };

    const limits = {
      x: this.ship.sprite.width - CANVAS.padding,
      y: this.ship.sprite.height - CANVAS.padding,
    };

    setLimitedUnitPosition(this.ship, {
      right: limits.x,
      left: limits.x,
      up: limits.y,
      down: limits.y,
    });
  };

  getLoop(nextScene: Scene): Function {
    let startGameText = this.getGameText(
      MESSAGES[LEVEL_TEXTS[this.state.level.id]]
    );
    this.ship.velocity = {
      x: 0,
      y: 0,
    };
    this.ship.alive = true;

    this.state.control.direction = this.ship.velocity;
    this.ship.sprite.position.set(CANVAS.width * 0.1, CANVAS.height * 0.5);
    this.ship.sprite.visible = true;

    this.container.addChild(this.ship.sprite);
    this.container.addChild(startGameText);
    this.container.visible = true;

    setTimeout(this.removeText, TIMEOUTS.startGameText, startGameText);
    setTimeout(this.spawnEnemy, this.state.level.enemy.spawnDelay);
    setTimeout(this.launchPlayerProjectile, this.state.level.fireDelay);
    setTimeout(
      this.randomizeEnemyMovement,
      this.state.level.enemy.dirChangeDelay
    );

    return () => {
      this.parallaxBackground.tilePosition.x -= PARALLAX_BACKGROUND_SPEED;
      this.parallaxForeground.tilePosition.x -= PARALLAX_FOREGROUND_SPEED;

      this.repositionShip();
      this.checkRocketBoundary();
      this.checkEnemyBoundary();
      this.checkRocketCollision();
      this.checkShipCollision(nextScene);
    };
  }
}
