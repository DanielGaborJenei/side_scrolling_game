import { OpeningScene } from './opening';
import { MenuScene } from './menu';
import { MissionScene } from './mission';
import { CollisionTestScene } from './collisionTestScene';

export default (state: State) => {
  return function() {
    const openingScene = new OpeningScene(state);
    const missionScene = new MissionScene(state);
    const collisionTestScene = new CollisionTestScene(state);
    const menuScene = new MenuScene(state, missionScene, collisionTestScene);

    state.app.ticker.add(() => state.loop());
    state.loop = openingScene.getLoop(menuScene);
  };
};
