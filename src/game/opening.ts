import { TIMEOUTS } from '../configuration';
import { newPixiSprite, removeChildWithFadeOut } from '../utilities';

export class OpeningScene {
  state: State;
  container: PIXI.Container = new PIXI.Container();
  background: PIXI.Sprite = newPixiSprite('openingBackground');

  constructor(state: State) {
    this.state = state;
    this.container.addChild(this.background);
    state.app.stage.addChild(this.container);
  }

  getLoop(nextScene: Scene): Function {
    setTimeout(
      removeChildWithFadeOut,
      TIMEOUTS.openingScene,
      this.background,
      this.container,
      TIMEOUTS.openingFadeout,
      false,
      (): void => {
        this.container.visible = false;
        this.state.loop = nextScene.getLoop();
      }
    );

    return () => {};
  }
}
