import { createText, newPixiSprite } from '../utilities';
import { MissionScene } from './mission';
import {
  BUTTON_TEXT_STYLE_DEFAULT,
  BUTTON_TEXT_STYLE_SMALL,
  CANVAS,
  LEVELS_CONFIG,
  MESSAGES,
  MENU_LAYOUT,
} from '../configuration';
import {
  addMenuShipChild,
  menuShipAnimationInit,
  menuShipBoundaryCheck,
} from './menuAnimation';
import { CollisionTestScene } from './collisionTestScene';

export class MenuScene {
  container: PIXI.Container = new PIXI.Container();

  constructor(
    state: State,
    missionScene: MissionScene,
    collisionTestScene: CollisionTestScene
  ) {
    let logo = newPixiSprite('logo');
    let background = newPixiSprite('menuBackground');

    const startLevel = (level: string): void => {
      this.container.visible = false;
      state.level = LEVELS_CONFIG[level];
      state.loop = missionScene.getLoop(this);
    };

    const startCollisionTest = (): void => {
      this.container.visible = false;
      state.loop = collisionTestScene.getLoop(this);
    };

    const onGame1ButtonDown = (): void => startLevel('easy');
    const onGame2ButtonDown = (): void => startLevel('normal');
    const onGame3ButtonDown = (): void => startLevel('hard');

    const onExitButtonDown = (): void => {
      this.container.visible = false;
      window.location.href = '//www.pixijs.com/';
    };

    const onCollisionTestButtonDown = (): void => startCollisionTest();

    const addButtonChild = (
      buttonID: string,
      textID: string,
      posX: number,
      posY: number,
      buttonCallback: Function,
      style: TextStyle = BUTTON_TEXT_STYLE_DEFAULT
    ): void => {
      let button = newPixiSprite(buttonID);

      button.anchor.set(0.5);
      button.position.set(posX, posY);
      button.interactive = true;
      button.buttonMode = true;
      button.on('pointerdown', buttonCallback);

      button.addChild(createText(MESSAGES[textID], style));

      this.container.addChild(button);
    };

    logo.anchor.set(0, 1);
    logo.scale.set(0.75);
    logo.rotation = Math.PI / 2;
    logo.position.set(MENU_LAYOUT.logo.x, MENU_LAYOUT.logo.y);

    background.anchor.set(0.5, 0.5);
    background.position.set(CANVAS.width * 0.5, CANVAS.height * 0.5);

    this.container.addChild(background);
    this.container.addChild(logo);
    addMenuShipChild(this.container);

    addButtonChild(
      'button',
      'GAME1',
      MENU_LAYOUT.button.x,
      MENU_LAYOUT.button.y,
      onGame1ButtonDown
    );
    addButtonChild(
      'button',
      'GAME2',
      MENU_LAYOUT.button.x,
      MENU_LAYOUT.button.y + 60,
      onGame2ButtonDown
    );
    addButtonChild(
      'button',
      'GAME3',
      MENU_LAYOUT.button.x,
      MENU_LAYOUT.button.y + 120,
      onGame3ButtonDown
    );
    addButtonChild(
      'exitButton',
      'EXIT',
      MENU_LAYOUT.button.x,
      MENU_LAYOUT.button.y + 180,
      onExitButtonDown
    );
    addButtonChild(
      'button',
      'COLLISION',
      MENU_LAYOUT.button.x + 580,
      MENU_LAYOUT.button.y + 362.5,
      onCollisionTestButtonDown,
      BUTTON_TEXT_STYLE_SMALL
    );

    this.container.visible = false;
    state.app.stage.addChild(this.container);
  }

  menuAnimationInit = menuShipAnimationInit;
  menuAnimationLoop = menuShipBoundaryCheck;

  getLoop(): Function {
    this.container.visible = true;
    this.menuAnimationInit(this.container);

    return () => {
      this.menuAnimationLoop();
    };
  }
}
