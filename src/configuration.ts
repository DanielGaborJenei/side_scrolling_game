export const RESOURCES_PATH = 'resources';
export const PARALLAX_BACKGROUND_SPEED = 0.2;
export const PARALLAX_FOREGROUND_SPEED = 0.6;

export const LEVELS_CONFIG: { [key: string]: Level } = {
  easy: {
    id: 'easy',
    shipSpeed: 4,
    fireDelay: 300,
    enemy: {
      speed: 5,
      initialSpeed: -5,
      spawnDelay: 2000,
      dirChangeDelay: 1600,
    },
  },
  normal: {
    id: 'normal',
    shipSpeed: 6,
    fireDelay: 400,
    enemy: {
      speed: 6,
      initialSpeed: -8,
      spawnDelay: 1000,
      dirChangeDelay: 1200,
    },
  },
  hard: {
    id: 'hard',
    shipSpeed: 8,
    fireDelay: 350,
    enemy: {
      speed: 7,
      initialSpeed: -10,
      spawnDelay: 500,
      dirChangeDelay: 400,
    },
  },
};

export const MENU_LAYOUT = {
  button: {
    x: 125,
    y: 180,
  },
  logo: {
    x: 50,
    y: 40,
  },
};

export const CANVAS = {
  padding: 20,
  width: 800,
  height: 600,
};

export const EXPLOSION = {
  particles: {
    ship: 50,
    enemy: 20,
  },
  duration: {
    ship: 5000,
    enemy: 800,
  },
};

export const FADEOUT_FPS = 30;

export const TIMEOUTS = {
  menuShipDirectionChange: 2000,
  startGameText: 1500,
  openingScene: 2000,
  openingFadeout: 2000,
  endScene: 4000,
};

// Preparing for i18n
export const MESSAGES: { [key: string]: string } = {
  GAME1: 'GAME1',
  GAME2: 'GAME2',
  GAME3: 'GAME3',
  EXIT: 'EXIT',
  COLLISION: 'COLLISION TEST',
  MENU: 'MENU',
  PRESS_ESC: 'PRESS ESC',
  'GAME OVER': 'GAME OVER',
  LEVEL: 'LEVEL',
  EASY: 'EASY',
  NORMAL: 'NORMAL',
  HARD: 'HARD',
};

export const MISSION_MAIN_TEXT_STYLE: TextStyle = {
  size: 72,
  fill: '#7F0000',
  align: 'center',
  anchor: {
    x: 0.5,
    y: 0.5,
  },
  position: {
    x: CANVAS.width / 2,
    y: CANVAS.height / 2,
  },
};

export const INFO_TEXT_STYLE: TextStyle = {
  size: 24,
  fill: '#FF0000',
  align: 'right',
  anchor: {
    x: 1,
    y: 1,
  },
  position: {
    x: CANVAS.width * 0.95,
    y: CANVAS.height * 0.95,
  },
};

export const BUTTON_TEXT_STYLE_DEFAULT: TextStyle = {
  size: 28,
  fill: 'white',
  align: 'center',
  anchor: {
    x: 0.5,
    y: 0.5,
  },
  position: {
    x: 0,
    y: -3,
  },
};

export const BUTTON_TEXT_STYLE_SMALL: TextStyle = {
  size: 16,
  fill: 'white',
  align: 'center',
  anchor: {
    x: 0.5,
    y: 0.5,
  },
  position: {
    x: 0,
    y: -1.8,
  },
};

export const LEVEL_TEXTS: { [key: string]: string } = {
  easy: 'EASY',
  normal: 'NORMAL',
  hard: 'HARD',
};

export const KEYMAP = {
  up: 'KeyW',
  down: 'KeyS',
  left: 'KeyA',
  right: 'KeyD',
  quit: 'Escape',
};

export const RESOURCES: { [key: string]: string } = {
  openingBackground: 'opening_bg.png',
  menuBackground: 'menu_bg.png',
  cityForeground: 'city_fg.png',
  cityBackground: 'city_bg.png',
  button: 'button.png',
  ship: 'ship.png',
  rocket: 'rocket.png',
  enemy: 'enemy.png',
  exitButton: 'button_exit.png',
  spark: 'spark.png',
  logo: 'logo.png',
};
