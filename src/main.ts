import { CANVAS, LEVELS_CONFIG } from './configuration';
import { getResource } from './utilities';
import initInputHandlers from './input_handlers';
import game from './game/game';

export const run = function(
  pixi: typeof PIXI,
  parentElement: HTMLElement
): void {
  const state: State = {
    loop: () => {},
    app: new pixi.Application({
      width: CANVAS.width,
      height: CANVAS.height,
    }),
    level: LEVELS_CONFIG.normal,
    control: {
      direction: {
        x: 0,
        y: 0,
      },
      activated: {
        left: false,
        up: false,
        right: false,
        down: false,
        quit: false,
      },
    },
  };

  initInputHandlers(state.control);
  parentElement.appendChild(state.app.view);

  pixi.loader
    .add(getResource('openingBackground'))
    .add(getResource('menuBackground'))
    .add(getResource('cityForeground'))
    .add(getResource('cityBackground'))
    .add(getResource('button'))
    .add(getResource('ship'))
    .add(getResource('rocket'))
    .add(getResource('enemy'))
    .add(getResource('exitButton'))
    .add(getResource('spark'))
    .add(getResource('logo'))
    .load(game(state));
};
