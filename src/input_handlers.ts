import { KEYMAP } from './configuration';

export default function(control: Control): void {
  window.onkeydown = function(e): void {
    switch (e.code) {
      case KEYMAP.up:
        control.direction.y = -1;
        control.activated.up = true;
        break;
      case KEYMAP.down:
        control.direction.y = 1;
        control.activated.down = true;
        break;
      case KEYMAP.left:
        control.direction.x = -1;
        control.activated.left = true;
        break;
      case KEYMAP.right:
        control.direction.x = 1;
        control.activated.right = true;
        break;
      case KEYMAP.quit:
        control.activated.quit = true;
        break;
    }
  };

  window.onkeyup = function(e): void {
    switch (e.code) {
      case KEYMAP.up:
        control.activated.up = false;
        control.direction.y = control.activated.down ? 1 : control.direction.y;
        break;
      case KEYMAP.down:
        control.activated.down = false;
        control.direction.y = control.activated.up ? -1 : control.direction.y;
        break;
      case KEYMAP.left:
        control.activated.left = false;
        control.direction.x = control.activated.right ? 1 : control.direction.x;
        break;
      case KEYMAP.right:
        control.activated.right = false;
        control.direction.x = control.activated.left ? -1 : control.direction.x;
        break;
    }

    control.direction.x =
      control.activated.left || control.activated.right
        ? control.direction.x
        : 0;
    control.direction.y =
      control.activated.up || control.activated.down ? control.direction.y : 0;
  };
}
