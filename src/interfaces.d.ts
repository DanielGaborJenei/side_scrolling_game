type Vector = {
  x: number;
  y: number;
};

type Coordinate = Vector;
type Velocity = Vector;
type Direction = Vector;

declare interface PositionLimit {
  up: number;
  down: number;
  right: number;
  left: number;
}

declare interface Unit {
  sprite: PIXI.Sprite;
  velocity: Velocity;
  alive?: boolean;
}

declare interface Level {
  id: string;
  shipSpeed: number;
  fireDelay: number;
  enemy: {
    speed: number;
    initialSpeed: number;
    spawnDelay: number;
    dirChangeDelay: number;
  };
}

declare interface TextStyle {
  size: number;
  fill: string;
  align: string;
  anchor: Coordinate;
  position: Coordinate;
}

declare interface Step {
  sprite: PIXI.Sprite;
  container: PIXI.Container;
  duration: number;
  delay: number;
  fadeAmount: number;
  doesScaleOut: boolean;
  finalStep: Function;
  fadeAge?: number;
}

declare interface Control {
  direction: Direction;
  activated: {
    left: boolean;
    up: boolean;
    right: boolean;
    down: boolean;
    quit: boolean;
  };
}

declare interface State {
  app: PIXI.Application;
  loop: Function;
  level: Level;
  control: Control;
}

declare interface Scene {
  getLoop: Function;
}
