import {
  CANVAS,
  FADEOUT_FPS,
  RESOURCES,
  RESOURCES_PATH,
} from './configuration';

export const getResource = (resource: string): string =>
  `${RESOURCES_PATH}/${RESOURCES[resource]}`;

export const getRandomRange = (): number => Math.random() * 2 - 1;

export const getCorrectedSpeed = (
  position: number,
  defaultSpeed: number,
  newSpeed: number,
  highLimit: number,
  lowLimit: number
): number =>
  position > highLimit
    ? -newSpeed
    : position < lowLimit
    ? newSpeed
    : defaultSpeed;

export const limiter = (
  value: number,
  maxLimit: number,
  minLimit: number
): number => Math.min(Math.max(value, minLimit), maxLimit);

export const calculateLimitedUnitPosition = (
  unit: Unit,
  limits: PositionLimit
): number[] => [
  limiter(
    unit.sprite.position.x + unit.velocity.x,
    CANVAS.width - limits.right,
    limits.left
  ),
  limiter(
    unit.sprite.y + unit.velocity.y,
    CANVAS.height - limits.down,
    limits.up
  ),
];

export const setLimitedUnitPosition = (
  unit: Unit,
  limits: PositionLimit
): void => {
  unit.sprite.position.set(...calculateLimitedUnitPosition(unit, limits));
};

export const newPixiSprite = (resource: string): PIXI.Sprite =>
  PIXI.Sprite.fromImage(getResource(resource));

export const newPixiTexture = (resource: string): PIXI.Texture =>
  PIXI.Texture.fromImage(getResource(resource));

export const createText = (
  text: string,
  { size, fill, align, anchor, position }: TextStyle
): PIXI.Text => {
  let textElement = new PIXI.Text(text, {
    fontFamily: 'Silkscreen',
    fontSize: size,
    fill: fill,
    align: align,
  });
  textElement.anchor.set(anchor.x, anchor.y);
  textElement.position.set(position.x, position.y);

  return textElement;
};

export const fadeStep = (step: Step): void => {
  let {
    sprite,
    container,
    duration,
    delay,
    doesScaleOut,
    finalStep,
    fadeAmount,
  } = step;

  step.fadeAge = (step.fadeAge || 0) + delay;
  sprite.alpha -= fadeAmount;

  if (doesScaleOut) {
    sprite.scale.set(sprite.alpha);
  }

  if (step.fadeAge < duration) {
    setTimeout(fadeStep, delay, step);
  } else {
    container.removeChild(sprite);
    finalStep && finalStep();
  }
};

export const removeChildWithFadeOut = (
  sprite: PIXI.Sprite,
  container: PIXI.Container,
  duration: number,
  doesScaleOut: boolean = true,
  finalStep?: Function
): void => {
  const delay = 1000 / FADEOUT_FPS;
  let fadeAmount = FADEOUT_FPS / duration;

  const step: Step = {
    sprite,
    container,
    duration,
    delay,
    doesScaleOut,
    finalStep,
    fadeAmount,
  };

  setTimeout(fadeStep, delay, step);
};

export const shiftPositionBy = (
  sprite: PIXI.Sprite,
  velocity: Velocity
): { (): void } => {
  return (): void => {
    sprite.position.set(
      sprite.position.x + velocity.x,
      sprite.position.y + velocity.y
    );
  };
};

export const explosion = (
  app: PIXI.Application,
  container: PIXI.Container,
  coordinate: Coordinate,
  maxParticles: number,
  speed: number,
  duration: number
): void => {
  for (let i = 0; i < maxParticles; ++i) {
    let velocity: Velocity = {
      x: getRandomRange() * speed,
      y: getRandomRange() * speed,
    };
    let sprite = newPixiSprite('spark');

    sprite.position.set(coordinate.x, coordinate.y);
    app.ticker.add(shiftPositionBy(sprite, velocity));
    container.addChild(sprite);

    removeChildWithFadeOut(sprite, container, duration, true);
  }
};

export const addUnit = (
  scene: PIXI.Container,
  texture: string,
  collection: Unit[],
  coordinate: Coordinate,
  velocity: Velocity
): void => {
  let unit: Unit = {
    sprite: newPixiSprite(texture),
    velocity: {
      x: velocity.x,
      y: velocity.y,
    },
    alive: true,
  };
  unit.sprite.position.set(coordinate.x, coordinate.y);
  collection.push(unit);
  scene.addChild(unit.sprite);
};

export const addBackgroundChild = (
  texture: PIXI.Texture,
  container: PIXI.Container
): PIXI.extras.TilingSprite => {
  let background = new PIXI.extras.TilingSprite(
    texture,
    CANVAS.width,
    CANVAS.height
  );

  container.addChild(background);

  return background;
};

export const collisionDetection = (
  s1: PIXI.Sprite,
  s2: PIXI.Sprite
): boolean => {
  let diagonal = {
    first: { width: s1.width / 2, height: s1.height / 2 },
    second: { width: s2.width / 2, height: s2.height / 2 },
  };
  let distX: number = Math.abs(
    s1.position.x - s2.position.x + diagonal.first.width - diagonal.second.width
  );
  let distY: number = Math.abs(
    s1.position.y -
      s2.position.y +
      diagonal.first.height -
      diagonal.second.height
  );
  let maxDistX: number = diagonal.first.width + diagonal.second.width;
  let maxDistY: number = diagonal.first.height + diagonal.second.height;

  return distX < maxDistX && distY < maxDistY;
};
