module.exports = {
  entry: './src/main.ts',
  output: {
    filename: 'scroller.js',
    library: 'scrollerGame',
    libraryTarget: 'var',
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [{ loaders: 'ts-loader' }],
  },
};
